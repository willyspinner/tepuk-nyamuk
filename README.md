# The Tepuk Nyamuk game

## What is it?

Tepuk nyamuk is an indonesian (or maybe also elsewhere?) card game which is very fun to play!. How it works is as follows:
* Everyone gets given out a personal pile of cards. They are not to look at their own pile, and must be faced down.
* People count in order (Ace to King) as they throw a single card to a central pile from their own pile.
* if the count matches the card's number, then everyone has to slap the central pile. The last person to slap this central pile has to get all the cards from the center.
* First person to finish their own pile, and successfuly slaps three times wins!

## How to play

To host, you simply have to:
```sh
cd tepuk-nyamuk-game-folder
npm start
```

And then the players can simply access the game by browser as the game is hosted by LAN.

### controls

press `t` to throw a card from your own pile, and `space` to slap the center pile. Careful not to slap the center pile unless the card and counter matches!
